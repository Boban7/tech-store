import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CardViewComponent } from './card-view/card-view.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { CardDialogComponent } from './card-dialog/card-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CardListComponent } from './card-list/card-list.component';
import { MatSelectModule } from '@angular/material/select';
import { FormControlName } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CardViewComponent, CardDialogComponent, CardListComponent],

  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [CardViewComponent, CardListComponent],
})
export class SharedModule {}

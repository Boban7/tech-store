import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CardDialogComponent } from '../card-dialog/card-dialog.component';
import { Article } from '../shared-model';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],
})
export class CardListComponent implements OnInit {
  @Input() products: Article;

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openDetails() {
    let dialog_config: MatDialogConfig<Article> =
      new MatDialogConfig<Article>();
    dialog_config.data = this.products;

    const dialogRef = this.dialog.open(CardDialogComponent, dialog_config);
  }
}

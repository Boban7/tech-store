export class Article {
  constructor(
    public title?: string,
    public image?: string,
    public text?: string,
    public price?: string,
    public manufacturer?: string,
    public ram?: string,
    public display?: string,
    public storage?: string
  ) {}
}

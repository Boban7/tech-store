import { Component, Input, OnInit } from '@angular/core';
import { Article } from '../shared-model';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CardDialogComponent } from '../card-dialog/card-dialog.component';

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss'],
})
export class CardViewComponent implements OnInit {
  @Input() products: Article;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  openDetails() {
    let dialog_config: MatDialogConfig<Article> =
      new MatDialogConfig<Article>();
    dialog_config.data = this.products;

    const dialogRef = this.dialog.open(CardDialogComponent, dialog_config);
  }
}

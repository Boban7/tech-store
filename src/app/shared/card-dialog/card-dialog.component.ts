import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Article } from '../shared-model';

@Component({
  selector: 'app-card-dialog',
  templateUrl: './card-dialog.component.html',
  styleUrls: ['./card-dialog.component.scss'],
})
export class CardDialogComponent implements OnInit {
  product: Article = new Article();
  constructor(
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private _data: Article
  ) {
    this.product = this._data;
  }

  ngOnInit(): void {}
}

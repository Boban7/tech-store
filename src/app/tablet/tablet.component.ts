import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../shared/shared-model';
import { TabletService } from './tablet.service';

@Component({
  selector: 'app-tablet',
  templateUrl: './tablet.component.html',
  styleUrls: ['./tablet.component.scss'],
})
export class TabletComponent implements OnInit {
  public tablet: Article[];
  is_table_view: boolean = false;

  whole_tablet_list: Article[] = [];

  constructor(
    private _tabletService: TabletService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._route.queryParams.subscribe((params: any) => {
      if (params.table_view) {
        this.is_table_view =
          params.table_view && params.table_view == 'true' ? true : false;
      } else {
        this._router.navigate([], {
          queryParams: {
            table_view: false,
          },
        });
      }
    });

    this.whole_tablet_list = this._tabletService.getArticles();
    this.tablet = this.whole_tablet_list;

    this._tabletService.filtered_tablet.subscribe((selected_tablet: string) => {
      if (selected_tablet == null) {
        this.tablet = this.whole_tablet_list;
      } else {
        this.tablet = this.whole_tablet_list.filter(
          (single_tablet: Article) =>
            single_tablet.manufacturer.toLowerCase() ==
            selected_tablet.toLowerCase()
        );
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Article } from '../shared/shared-model';

@Injectable({
  providedIn: 'root',
})
export class TabletService {
  filtered_tablet: Subject<string> = new Subject<string>();

  public tablet: Article[] = [
    {
      title: 'Tablet PC Mediacom SmartPad Iyo 10 4G Octa 1.6GHz/2GB/16GB',
      price: '6,680 ден',
      manufacturer: 'Mediacom',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/msp1ey4g_1.jpg',
      ram: ' 2GB Soldered',
      display: '7" HD (1024x600) IPS',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Huawei MatePad T10 2GB/32GB',
      price: '6,980 ден',
      manufacturer: 'Huawei',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/13082021112731matepadt10-1.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title:
        'Tablet PC Lenovo Tab M8 TB-8505X 4G MediaTek Helio 2.0GHz/2GB/32GB',
      price: '7,380 ден',
      manufacturer: 'Lenovo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/za5h0016bg.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Blackview Tab 8E Octa 1.6GHz/3GB/32GB',
      price: '8,280 ден',
      manufacturer: 'Blackview',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/tab8egold_1.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title:
        'Tablet PC Samsung Galaxy Tab A T295 LTE-4G QuadCore 2.0GHz/2GB/32GB',
      price: '8,580 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/smt295silver.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Amazon Kindle Fire HD 10 2021 32GB',
      price: '9,980 ден',
      manufacturer: 'Amazon',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/amazonfirehd10_2021_1.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Huawei MatePad T10 LTE 9.7" IPS OctaCore 4GB/64GB',
      price: '10,580 ден',
      manufacturer: 'Huawei',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/ni45414812c1s4541481201.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title:
        'Tablet PC Samsung Galaxy Tab A8 SM-X205 LTE 4G 2021 10.5" OctaCore 4GB/64GB',
      price: '16,980 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/x205-silver.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Xiaomi Pad 5 6GB/128GB 11"',
      price: '19,580 ден',
      manufacturer: 'Xiaomi',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/vhu4088eu_1.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Apple iPad Air 4 2020 64GB ',
      price: '42,980 ден',
      manufacturer: 'Apple',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/ipad_air_2020_rg.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Samsung Tab S7 FE SM-T736 5G 4GB/64GB',
      price: '37,980 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/smt736_1.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
    {
      title: 'Tablet PC Apple iPad 9 2021 64GB',
      price: '31,490 ден',
      manufacturer: 'Apple',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/ipad_2021_grey.jpg',
      ram: ' 2GB',
      display: ' 9.7 inches',
      storage: '32GB',
    },
  ];

  public getArticles(): Article[] {
    return this.tablet;
  }

  constructor() {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabletComponent } from './tablet.component';
import { RouterModule, Routes } from '@angular/router';

const tabletRoutes: Routes = [
  {
    path: '',
    component: TabletComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(tabletRoutes)],
  exports: [RouterModule],
})
export class TabletRoutingModule {}

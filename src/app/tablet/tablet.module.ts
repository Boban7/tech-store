import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabletRoutingModule } from './tablet-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TabletComponent } from './tablet.component';

@NgModule({
  declarations: [TabletComponent],
  imports: [CommonModule, TabletRoutingModule, SharedModule],
})
export class TabletModule {}

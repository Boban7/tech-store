import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TelefoniComponent } from './telefoni.component';

const telefoniRoutes: Routes = [
  {
    path: '',
    component: TelefoniComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(telefoniRoutes)],
  exports: [RouterModule],
})
export class TelefoniRoutingModule {}

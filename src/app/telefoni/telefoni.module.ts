import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelefoniRoutingModule } from './telefoni-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TelefoniComponent } from './telefoni.component';

@NgModule({
  declarations: [TelefoniComponent],
  imports: [CommonModule, TelefoniRoutingModule, SharedModule],
})
export class TelefoniModule {}

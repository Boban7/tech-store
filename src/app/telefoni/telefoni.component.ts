import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../shared/shared-model';
import { TelefoniService } from './telefoni.service';

@Component({
  selector: 'app-telefoni',
  templateUrl: './telefoni.component.html',
  styleUrls: ['./telefoni.component.scss'],
})
export class TelefoniComponent implements OnInit {
  public telefoni: Article[];
  is_table_view: boolean = false;
  whole_telefon_list: Article[] = [];
  constructor(
    private _telefoniService: TelefoniService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._route.queryParams.subscribe((params: any) => {
      if (params.table_view) {
        this.is_table_view =
          params.table_view && params.table_view == 'true' ? true : false;
      } else {
        this._router.navigate([], {
          queryParams: {
            table_view: false,
          },
        });
      }
    });

    this.whole_telefon_list = this._telefoniService.getArticles();
    this.telefoni = this.whole_telefon_list;

    this._telefoniService.filtered_telefon.subscribe(
      (selected_telefon: string) => {
        if (selected_telefon == null) {
          this.telefoni = this.whole_telefon_list;
        } else {
          this.telefoni = this.whole_telefon_list.filter(
            (single_telefon: Article) =>
              single_telefon.manufacturer.toLowerCase() ==
              selected_telefon.toLowerCase()
          );
        }
      }
    );
  }
}

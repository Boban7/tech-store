import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Article } from '../shared/shared-model';

@Injectable({
  providedIn: 'root',
})
export class TelefoniService {
  filtered_telefon: Subject<string> = new Subject<string>();

  public telefoni: Article[] = [
    {
      title: 'Samsung Galaxy A22 5G 4GB/64GB',
      price: '13,380 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/sma22gray5g-1.jpg',
      ram: '  6 GB RAM',
      display: '6.5 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Xiaomi Poco M4 Pro 4GB/64GB 5G Dual SIM Black',
      price: '14,980 ден',
      manufacturer: 'Xiaomi',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/pocom4pro_gr_1.jpg',
      ram: '  6 GB RAM',
      display: '6.3 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Samsung Galaxy A52 A525F 6.5" 6GB/128GB Dual SIM Black',
      price: '18,980 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/gsmarena_001.jpg',
      ram: '  8 GB RAM',
      display: '6.5 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Xiaomi 11 Lite 5G NE 8GB/128GB Bubblegum Blue',
      price: '19,980 ден',
      manufacturer: 'Xiaomi',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/mzb09smeu_1.jpg',
      ram: '  8 GB RAM',
      display: '6.78 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Samsung Galaxy S20 FE SM-G780 6GB/128GB Dual SIM Cloud Navy',
      price: '27,980 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/smg780gdscloudnavy_1.jpg',
      ram: '  8 GB RAM',
      display: '6.1 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Xiaomi 11T Pro 8GB/128GB Dual SIM Celestial Blue',
      price: '30,990 ден',
      manufacturer: 'Xiaomi',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/mzb09j3eu_1.jpg',
      ram: '  12 GB RAM',
      display: '6.78 inches FHD',
      storage: '256GB',
    },
    {
      title: 'Apple iPhone 11 64GB White',
      price: '36,990 ден',
      manufacturer: 'Apple',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/iphone_11_white.jpg',
      ram: '  6 GB RAM',
      display: '6.1 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Huawei P50 Pro Golden Black + FreeBuds 4i',
      price: '60,980 ден',
      manufacturer: 'Huawei',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/2-min(1).jpg',
      ram: '  8 GB RAM',
      display: '6.6 inches FHD',
      storage: '256GB',
    },
    {
      title: 'Apple iPhone 12 64GB Black"',
      price: '48,890 ден',
      manufacturer: 'Apple',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/iphone_12_black.jpg',
      ram: '  6 GB RAM',
      display: '6.3 inches FHD',
      storage: '256GB',
    },
    {
      title: 'Apple iPhone 13 512GB Midnight',
      price: '74,980 ден',
      manufacturer: 'Apple',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/mlml3lla_1.jpg',
      ram: '  6 GB RAM',
      display: '6.1 inches FHD',
      storage: '512GB',
    },
    {
      title: 'Apple iPhone 13 Pro Max 128GB Graphite',
      price: '83,980 ден',
      manufacturer: 'Apple',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/iphone_13promax_graphite.jpg',
      ram: '  4 GB RAM',
      display: '6.1 inches FHD',
      storage: '128GB',
    },
    {
      title: 'Samsung Galaxy S21 FE 5G 6GB/128GB DualSIM Light Violet',
      price: '43,780 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/s21fe-lavender_1.jpg',
      ram: '  8 GB RAM',
      display: '6.1 inches FHD',
      storage: '128GB',
    },
  ];
  public getArticles(): Article[] {
    return this.telefoni;
  }

  constructor() {}
}

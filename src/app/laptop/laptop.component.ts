import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { __importDefault } from 'tslib';
import { Article } from '../shared/shared-model';
import { LaptopService } from './laptop.service';

@Component({
  selector: 'app-laptop',
  templateUrl: './laptop.component.html',
  styleUrls: ['./laptop.component.scss'],
})
export class LaptopComponent implements OnInit {
  public laptop: Article[];
  is_table_view: boolean = false;

  private whole_laptop_list: Article[] = new Array<Article>();

  constructor(
    private _laptopService: LaptopService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._route.queryParams.subscribe((params: any) => {
      if (params.table_view) {
        this.is_table_view =
          params.table_view && params.table_view == 'true' ? true : false;
      } else {
        this._router.navigate([], {
          queryParams: {
            table_view: false,
          },
        });
      }
    });

    this.whole_laptop_list = this._laptopService.getArticles();
    this.laptop = this.whole_laptop_list;

    this._laptopService.filtered_laptop.subscribe((selected_laptop: string) => {
      if (selected_laptop == null) {
        this.laptop = this.whole_laptop_list;
      } else {
        this.laptop = this.whole_laptop_list.filter(
          (single_laptop: Article) =>
            single_laptop.manufacturer.toLowerCase() ==
            selected_laptop.toLowerCase()
        );
      }
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LaptopComponent } from './laptop.component';
import { CardListComponent } from '../shared/card-list/card-list.component';
import { CardViewComponent } from '../shared/card-view/card-view.component';

const laptopRoutes: Routes = [
  {
    path: '',
    component: LaptopComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(laptopRoutes)],
  exports: [RouterModule],
})
export class LaptopRoutingModule {}

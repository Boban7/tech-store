import { Injectable } from '@angular/core';
import { AsyncSubject, Subject } from 'rxjs';
import { Article } from '../shared/shared-model';

@Injectable({
  providedIn: 'root',
})
export class LaptopService {
  filtered_laptop: Subject<string> = new Subject<string>();

  public laptop: Article[] = [
    {
      title: 'Notebook Samsung Chromebook 4 N4020/4GB/32GB SSD/11.6" HD LED',
      price: '9,940 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/xe310xbakc1us.jpg',
      ram: ' 4 GB LPDDR4',
      display: '11.6" HD LED Display (1366 x 768), Anti-Reflective (TN)',
      storage: '32GB eMMC',
    },
    {
      title: 'Notebook Lenovo IP3 15-ADA05 AMD 3020E 2.6Ghz 4GB/256GB',
      price: '16,980 ден',
      manufacturer: 'Lenovo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/756137529.lenovo-ideapad-3-81w100d5rm.jpg',
      ram: ' 6 GB LPDDR4',
      display: '14" HD LED Display (1366 x 768), Anti-Reflective (TN)',
      storage: '32GB eMMC',
    },
    {
      title: 'Notebook Asus L510MA N4020/4GB/128GB',
      price: '17,980 ден',
      manufacturer: 'Asus',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/l510mawb04.jpg',
      ram: ' 4 GB DDR4-2400 SDRAM',
      display: '15.6" diagonal, FHD',
      storage: '128 GB ',
    },
    {
      title: 'Notebook HP 250 G8 i3-1115G4 8GB/256GB',
      price: '23,980 ден',
      manufacturer: 'HP',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/120220221305061.jpg',
      ram: ' 8 GB DDR4-2400 SDRAM',
      display: '15.6" 	FHD (1920 x 1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Huawei D15 i3-10110U 8GB/256GB',
      price: '25,980 ден',
      manufacturer: 'Huawei',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/9981635526686.jpg',
      ram: ' 8 GB DDR4-2400 SDRAM',
      display: '15.6" 	FHD (1920 x 1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Dell Vostro 3515 Ryzen5 3450U 8GB/256GB',
      price: ' 28,980 ден',
      manufacturer: 'Dell',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/vostro3515r5_1.jpg',
      ram: ' 8 GB DDR4-2400 SDRAM',
      display: '15.6" 	FHD (1920 x 1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook HP Pavilion Ryzen7 5700U/8GB/512GB',
      price: '42,980 ден',
      manufacturer: 'HP',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/364k5ua.jpg',
      ram: ' 8 GB DDR4-2400 SDRAM',
      display: '15.6" 	FHD (1920 x 1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Lenovo 5 Ryzen7 4700U/8GB/512GB',
      price: '45,980 ден',
      manufacturer: 'Lenovo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/81yq0006us_1.jpg',
      ram: ' 6 GB DDR4-2400 SDRAM',
      display: '15.6" 	FHD (1920 x 1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Asus Zenbook UX425 Ryzen 5600H/8GB/512GB',
      price: '51,980 ден',
      manufacturer: 'Asus',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/ux425eaki573t.jpg',
      ram: '8GB DDR4 2666MHz',
      display: '14" FHD (1920x1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Lenovo Yoga 7 14ITL5 i7-1165G7 16GB/512GB',
      price: '70,980 ден',
      manufacturer: 'Lenovo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/364k5ua.jpg',
      ram: '8GB DDR4 2666MHz',
      display: '14" FHD (1920x1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Lenovo Legion S7 Gaming Ryzen9 5900HX 32GB/1TB',
      price: '99,980 ден',
      manufacturer: 'Lenovo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/82k80027rm_1.jpg',
      ram: '8GB DDR4 2666MHz',
      display: '14" FHD (1920x1080)',
      storage: '256GB SSD',
    },
    {
      title: 'Notebook Asus ROG Strix Scar Ryzen9 5900HX/16GB',
      price: '149,980',
      manufacturer: 'Asus',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/g533qrhq080t.jpg',
      ram: '8GB DDR4 2666MHz',
      display: '14" FHD (1920x1080)',
      storage: '256GB SSD',
    },
  ];

  public getArticles(): Article[] {
    return this.laptop;
  }

  constructor() {}
}

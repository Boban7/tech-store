import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LaptopRoutingModule } from './laptop-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CardViewComponent } from '../shared/card-view/card-view.component';
import { LaptopComponent } from './laptop.component';
import { CardHeaderComponent } from '../card-header/card-header.component';

@NgModule({
  declarations: [LaptopComponent],
  imports: [CommonModule, LaptopRoutingModule, SharedModule],
})
export class LaptopModule {}

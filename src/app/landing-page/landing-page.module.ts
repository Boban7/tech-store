import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './landing-page.component';
import { HeaderModule } from '../shared/header/header.module';
import { LandingPageRoutingModule } from './landing-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, LandingPageRoutingModule, HeaderModule],
})
export class LandingPageModule {}

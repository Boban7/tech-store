import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardViewComponent } from '../shared/card-view/card-view.component';
import { CardListComponent } from '../shared/card-list/card-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng5SliderModule } from 'ng5-slider';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSelectModule,
    ReactiveFormsModule,
    Ng5SliderModule,
  ],
})
export class CardHeaderModule {}

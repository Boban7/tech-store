import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Article } from '../shared/shared-model';
import { NgForm } from '@angular/forms';
import { LabelType, Options } from 'ng5-slider';
import { LaptopService } from '../laptop/laptop.service';
import { filter, single } from 'rxjs';
import { TelefoniService } from '../telefoni/telefoni.service';
import { TabletService } from '../tablet/tablet.service';
import { TelevizoriService } from '../televizori/televizori.service';

@Component({
  selector: 'app-card-header',
  templateUrl: './card-header.component.html',
  styleUrls: ['./card-header.component.scss'],
})
export class CardHeaderComponent implements OnInit {
  article: string;

  // @Input() proizvoditel: Article;

  // articles: Article = new Article();

  public articles: Article[];

  selected_category: string = 'laptop';
  whole_article_list: Array<Article> = new Array<Article>();

  // onDisplayModeChange(mode: number): void {
  //   this.displayMode = mode;
  // }

  constructor(
    private _router: Router,
    private _laptopService: LaptopService,
    private _telefoniService: TelefoniService,
    private _tabletService: TabletService,
    private _televizoriService: TelevizoriService
  ) {}

  ngOnInit(): void {
    this.subscribeToRouteChanges();
  }

  subscribeToRouteChanges() {
    this._router.events
      .pipe(filter((router) => router instanceof NavigationEnd))
      .subscribe((router: NavigationEnd) => {
        console.log('router.url -> ', router.url);
        let splitted_slash_url: string = router.url.split('/')[1];
        console.log('splitted_slash_url -> ', splitted_slash_url);
        let splitted_question_mark_url: string =
          splitted_slash_url.split('?')[0];
        console.log(
          'splitted_question_mark_url -> ',
          splitted_question_mark_url
        );
        this.selectList(splitted_question_mark_url);
      });
  }

  selectList(url: string): void {
    switch (url) {
      case 'laptop':
        this.selected_category = 'laptop';
        this.whole_article_list = this._laptopService.getArticles();
        this.filterManufacturers();
        break;
      case 'telefoni':
        this.selected_category = 'telefoni';
        this.whole_article_list = this._telefoniService.getArticles();
        this.filterManufacturers();
        break;
      case 'tablet':
        this.selected_category = 'tableti';
        this.whole_article_list = this._tabletService.getArticles();
        this.filterManufacturers();
        break;
      case 'televizori':
        this.selected_category = 'televizori';
        this.whole_article_list = this._televizoriService.getArticles();
        this.filterManufacturers();
        break;
      default:
        this.selected_category = 'laptop';
        this.whole_article_list = this._laptopService.getArticles();
        this.filterManufacturers();
        break;
    }
  }

  filterManufacturers() {
    this.articles = new Array<Article>();
    this.whole_article_list.forEach((single_article: Article) => {
      let find_index: number = this.articles.findIndex(
        (article) =>
          article.manufacturer.toLowerCase() ==
          single_article.manufacturer.toLowerCase()
      );

      if (find_index < 0) {
        this.articles.push(single_article);
      }
    });
  }

  filterArticles() {
    let article_for_sending = this.article == 'null' ? null : this.article;

    if (this.selected_category == 'laptop') {
      this._laptopService.filtered_laptop.next(article_for_sending);
    } else if (this.selected_category == 'telefoni') {
      this._telefoniService.filtered_telefon.next(article_for_sending);
    } else if (this.selected_category == 'tableti') {
      this._tabletService.filtered_tablet.next(article_for_sending);
    } else if (this.selected_category == 'televizori') {
      this._televizoriService.filtered_televizor.next(article_for_sending);
    }
  }

  tableView(is_table_view: boolean) {
    this._router.navigate([], {
      queryParams: {
        table_view: is_table_view,
      },
    });
  }
}

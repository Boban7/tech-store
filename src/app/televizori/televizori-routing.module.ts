import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TelevizoriComponent } from './televizori.component';

const televizoriRoutes: Routes = [
  {
    path: '',
    component: TelevizoriComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(televizoriRoutes)],
  exports: [RouterModule],
})
export class TelevizoriRoutingModule {}

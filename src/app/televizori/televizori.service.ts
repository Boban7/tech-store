import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Article } from '../shared/shared-model';

@Injectable({
  providedIn: 'root',
})
export class TelevizoriService {
  filtered_televizor: Subject<string> = new Subject<string>();

  public televizori: Article[] = [
    {
      title: 'TV Neo 32" 32VB220 HD LED',
      price: '7,490 ден',
      manufacturer: 'Neo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/neo32vb.jpg',
    },
    {
      title: 'TV Samsung UE32T4002AK 32" LED HD',
      price: '9,990 ден',
      manufacturer: 'Samsung',
      image: 'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/610b.jpg',
    },
    {
      title: 'TV Philips 32PHS5505 32" HD LED',
      price: '11,490 ден',
      manufacturer: 'Philips',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/32phs550512.jpg',
    },
    {
      title: 'TV Sony KDL-32WE615 32" HD LED',
      price: '18,990 ден',
      manufacturer: 'Sony',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/05122019095849main.jpg',
    },
    {
      title: 'TV Philips 43PUS7805 4K Ultra HD SMART ',
      price: '19,990 ден',
      manufacturer: 'Philips',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/43pus780512.jpg',
    },
    {
      title: 'TV Neo 55" 55VUS820 4K Smart UHD LED',
      price: '21,990 ден',
      manufacturer: 'Neo',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/14052020125402neo50vnb.jpg',
    },
    {
      title: 'TV Philips 43PUS8505 43" 4K Android Ultra HD LED',
      price: '24,990 ден',
      manufacturer: 'Philips',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/43pus8505.jpg',
    },
    {
      title: 'TV Samsung UE50AU7172UX 50" Crystal 4K LED Smart',
      price: '30,990 ден',
      manufacturer: 'Samsung',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/260320212059350.jpg',
    },
    {
      title: 'TV Sony KD-55X80J 55" 4K Ultra HD Smart LED',
      price: '44,990 ден',
      manufacturer: 'Sony',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/1746562020.jpg',
    },
    {
      title: 'TV Sony KD-65X80J 65" 4K Ultra HD Smart LED',
      price: '54,990 ден',
      manufacturer: 'Sony',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/kd65x80jcep1.jpg',
    },
    {
      title: 'TV Sony XR-55X90J 55" 4K Ultra HD Smart LED',
      price: '59,990 ден',
      manufacturer: 'Sony',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/92294984.jpg',
    },
    {
      title: 'TV Sony KD-75X81J 75" 4K Ultra HD Smart LED',
      price: '79,990 ден',
      manufacturer: 'Sony',
      image:
        'https://d1a68gwbwfmqto.cloudfront.net/img/products/full/75x81j.jpg',
    },
  ];

  public getArticles(): Article[] {
    return this.televizori;
  }

  constructor() {}
}

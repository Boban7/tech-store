import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelevizoriRoutingModule } from './televizori-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TelevizoriComponent } from './televizori.component';

@NgModule({
  declarations: [TelevizoriComponent],
  imports: [CommonModule, TelevizoriRoutingModule, SharedModule],
})
export class TelevizoriModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../shared/shared-model';
import { TelevizoriService } from './televizori.service';

@Component({
  selector: 'app-televizori',
  templateUrl: './televizori.component.html',
  styleUrls: ['./televizori.component.scss'],
})
export class TelevizoriComponent implements OnInit {
  public televizori: Article[];
  is_table_view: boolean = false;
  whole_televizor_list: Article[] = [];

  constructor(
    private _televizoriService: TelevizoriService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._route.queryParams.subscribe((params: any) => {
      if (params.table_view) {
        this.is_table_view =
          params.table_view && params.table_view == 'true' ? true : false;
      } else {
        this._router.navigate([], {
          queryParams: {
            table_view: false,
          },
        });
      }
    });

    this.whole_televizor_list = this._televizoriService.getArticles();
    this.televizori = this.whole_televizor_list;

    this._televizoriService.filtered_televizor.subscribe(
      (selected_televizor: string) => {
        if (selected_televizor == null) {
          this.televizori = this.whole_televizor_list;
        } else {
          this.televizori = this.whole_televizor_list.filter(
            (single_telefon: Article) =>
              single_telefon.manufacturer.toLowerCase() ==
              selected_televizor.toLowerCase()
          );
        }
      }
    );
  }
}

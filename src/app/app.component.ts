import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Final';
  hideCardHeader: boolean = false;

  constructor(private _router: Router) {
    this._router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.hideCardHeader = val.urlAfterRedirects.startsWith('/landing');
      }
    });
  }
}

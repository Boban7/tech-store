import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'landing',
    loadChildren: () =>
      import('./landing-page/landing-page.module').then(
        (m) => m.LandingPageModule
      ),
  },
  {
    path: 'telefoni',
    loadChildren: () =>
      import('./telefoni/telefoni.module').then((m) => m.TelefoniModule),
  },
  {
    path: 'televizori',
    loadChildren: () =>
      import('./televizori/televizori.module').then((m) => m.TelevizoriModule),
  },
  {
    path: 'laptop',
    loadChildren: () =>
      import('./laptop/laptop.module').then((m) => m.LaptopModule),
  },
  {
    path: 'tablet',
    loadChildren: () =>
      import('./tablet/tablet.module').then((m) => m.TabletModule),
  },
  {
    path: '**',
    redirectTo: 'landing',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
